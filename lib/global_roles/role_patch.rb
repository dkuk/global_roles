module GlobalRoles
  module RolePatch
    def self.included(base)
      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)

      base.class_eval do
        has_many :global_roles, :dependent => :destroy
        has_many :principals_by_global_roles, :through => :global_roles,
                 :source => :principal,
                 :class_name => 'Principal'
        has_many :principals,
                 lambda { joins(members: :project).where("#{Principal.table_name}.status = #{Principal::STATUS_ACTIVE}")
                                                  .where("#{Project.table_name}.status = #{Project::STATUS_ACTIVE}")
                                                  .order("#{Principal.table_name}.type, #{Principal.table_name}.lastname")
                                                  .distinct
                        },
                 through: :members
      end
    end

    module ClassMethods

    end

    module InstanceMethods

    end

  end
end
